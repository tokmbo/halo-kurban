<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Animal;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $kurbanList = Animal::all();

        return view('user.home.index', compact('kurbanList'));
    }
}
