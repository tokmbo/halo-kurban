<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContact;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
        return view('user.contact.index');
    }

    public function post(StoreContact $request)
    {
        $message = Contact::create($request->validated());

        return response()->json([
            'data' => [
                'success' => true
            ]
        ]);
    }
}
