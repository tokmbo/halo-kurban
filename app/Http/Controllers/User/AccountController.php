<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        return view('user.home.index');
    }

    public function edit()
    {
        return view('user.account.edit');
    }

    public function update(Request $request)
    {

        dd($request->all());
        return view('user.account.edit');
    }
}
