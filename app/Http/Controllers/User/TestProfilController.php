<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Animal;
use Illuminate\Http\Request;

class TestProfilController extends Controller
{
    public function about($slug)
    {
    	$animal = Animal::where('slug', $slug)->firstOrFail();

        return view('user.testprofil.test', compact('animal'));
    }
}
