<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BeliKurbanController extends Controller
{
    public function about()
    {
        return view('user.BeliKurban.test');
    }
    public function detil()
    {
        return view('user.BeliKurban.detil');
    }
}
