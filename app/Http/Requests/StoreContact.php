<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:16',
            'lastname'  => 'nullable|max:16',
            'email'     => 'required|email|max:254',
            'subject'   => 'required|max:100',
            'note'      => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'firstname.required' => 'Nama depan tidak boleh kosong',
            'firstname.max'      => 'Nama depan maksimal 16 karakter',
            'lastname.max'       => 'Nama belakang maksimal 16 karakter',
            'email.required'     => 'Email tidak boleh kosong',
            'email.email'        => 'Format email salah',
            'email.max'          => 'Email terlalu panjang',
            'subject.required'   => 'Judul pesan tidak boleh kosong',
            'subject.max'        => 'Judul pesan maksimal 100 karakter',
            'note.required'      => 'Deskripsi pesan tidak boleh kosong',
        ];
    }
}
