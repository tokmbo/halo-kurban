<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'user.', 'namespace' => 'User'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/about', 'StaticController@about')->name('about');
    Route::get('/contact', 'ContactController@index')->name('contact.index');
    Route::post('/contact', 'ContactController@post')->name('contact.post');

    Route::get('/description/{slug}', 'TestProfilController@about')->name('test');

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('account/edit', 'AccountController@edit')->name('account.edit');
        Route::post('account/edit', 'AccountController@update')->name('account.edit');
    });
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');
