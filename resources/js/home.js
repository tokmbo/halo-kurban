import Vue from 'vue'

new Vue({
  el: '#app',

  data: {
    tes: tes,
  },

  mounted() {
  },

  methods: {
    initiatePreviousForm(isFormDisabled = false) {
      for (const key in this.award) {
        if (!this.award.hasOwnProperty(key)) {
            return
        }

        let prev = ($(`#${key}`).data('prev'))

        if (key == 'ig') {
          prev = !prev ? ($(`#done`).data('prev')) : null
          prev = !prev ? ($(`#nope`).data('prev')) : null

          $(":radio").prop('disabled', isFormDisabled)
        }

        if (prev) {
          this.award[key] = prev
        }

        $(`#${key}`).prop('disabled', isFormDisabled)
      }
    },

    submit() {
      if (this.award.favorite === 'Pilih HOP favorit') {
        this.award.favorite = null
      }

      var errors = validate(this.award, {
        favorite: {
          presence: {
            allowEmpty: false,
            message: '^Edisi favorit tidak boleh kosong'
          }
        }
      })

      if (this.award.ig === 'on') {
        var additional = validate(this.award, {
          post_url: {
            presence: {
              allowEmpty: false,
              message: '^Link post Instagram tidak boleh kosong'
            },
            url: {
              message: '^Link post Instagram tidak valid'
            }
          }
        })
      } else {
        var additional = validate(this.award, {
          image_url: {
            presence: {
              allowEmpty: false,
              message: '^Foto tidak boleh kosong'
            }
          }
        })
      }

      if (additional) {
        this.errors = Object.assign({}, errors, additional)
      } else {
        this.errors = errors
      }

      if (this.errors) {
        if (!additional) {
          this.award.favorite = 'Pilih HOP favorit'
        }

        return
      }

      $('#loading-modal').modal('show')

      $('#form-award').submit()
    },

    getSuggestChild() {
      if (this.award.child_id != null && this.children.length > 0) {
        let selectedChild = this.children.filter(child => child.id === this.award.child_id)

        if (selectedChild.length > 0) {
          selectedChild = selectedChild[0]
        }

        this.award.child_name = selectedChild.name
      }
    },

    uploadImage(e) {
      const file = e.target.files[0]

      if (file['type'] === 'image/jpeg' || file['type'] === 'image/png') {
        this.award.image_url = file
        this.imageUrl = URL.createObjectURL(file)
        this.errors = null
      } else {
        this.errors = {'image_url' : ['File harus bertipe gambar']}
      }
    },

    slideHere(el) {
      $('html, body').animate({
        scrollTop: $(el).offset().top - 15
      }, 500)
    },

    refreshPage() {
      $('#loading-modal').modal('show')

      window.location.reload(true)
    }
  }
})
