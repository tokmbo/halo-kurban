import Vue from 'vue'
import validate from 'validate.js'

var contact;

contact = new Vue({
  el: '#app',

  data: {
    message: {
      firstname: null,
      lastname: null,
      email: null,
      subject: null,
      note: null
    },
    token: null,
    errors: errors,
    success: null
  },

  beforeMount: function() {
    this.token = this.$el.querySelector('[name="_token"]').value;
  },

  methods: {
    submit() {
      this.openLoader()

      var errors = validate(this.message, {
        firstname: {
          presence: {
            allowEmpty: false,
            message: '^Nama depan tidak boleh kosong'
          },
          length: {
            maximum: 16,
            message: '^Nama depan maksimal 16 karakter'
          }
        },
        lastname: {
          length: {
            maximum: 16,
            message: '^Nama belakang maksimal 16 karakter'
          }
        },
        email: {
          presence: {
            allowEmpty: false,
            message: '^Email tidak boleh kosong'
          },
          email: {
            message: '^Format email salah'
          },
          length: {
            maximum: 254,
            message: '^Email terlalu'
          }
        },
        subject: {
          presence: {
            allowEmpty: false,
            message: '^Judul pesan tidak boleh kosong'
          },
          length: {
            maximum: 100,
            message: '^Judul pesan maksimal 100 karakter'
          }
        },
        note: {
          presence: {
            allowEmpty: false,
            message: '^Deskripsi pesan tidak boleh kosong'
          }
        },
      })

      this.errors = errors

      if (this.errors) {
        this.closeLoader()

        return
      }

      var data = new FormData();
          data.append('firstname', this.message.firstname);
          data.append('lastname', this.message.lastname);
          data.append('email', this.message.email);
          data.append('subject', this.message.subject);
          data.append('note', this.message.note);
          data.append('_token', this.token);

      $.ajax('/contact', {
          method: 'POST',
          data: data,
          processData: false,
          contentType: false,
          success: function (response) {
            contact.success = response.data.success

            contact.resetState()
          },
      });

      this.closeLoader()
    },

    openLoader() {
      $('.preloader').show()
    },

    closeLoader() {
      if($('.preloader').length) {
        $('.preloader').delay(100).fadeOut(500);
      }
    },

    slideHere(el) {
      $('html, body').animate({
        scrollTop: $(el).offset().top - 15
      }, 500)
    },

    resetState() {
      this.message.firstname = null
      this.message.lastname = null
      this.message.email = null
      this.message.subject = null
      this.message.note = null
    }
  }
})
