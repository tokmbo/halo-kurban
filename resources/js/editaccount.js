import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

var url = "https://dev.farizdotid.com/api/daerahindonesia/";

new Vue({
    el: '#app',

    data: {
        sources: {
            provinces: [],
            cities: [],
            districts: [],
            subdistricts: [],
            animals: {
                kambing: [
                    {
                        id: 1,
                        weightView: "25-28 Kg",
                        priceView: "1.800.000",
                        price: 1800000
                    },
                    {
                        id: 2,
                        weightView: "28-30 Kg",
                        priceView: "2.800.000",
                        price: 2800000
                    },
                    {
                        id:3,
                        weightView:"> 30 Kg",
                        perKg: 100.000,
                        priceView: "100.000",
                    }
                ],
                sapi: [
                    {
                        id: 4,
                        category: "Sapi Bali/Kupang/Madura",
                        weightView: "280-300 Kg",
                        price: 18000000,
                        priceView: "18 jt"
                    },
                    {
                        id: 5,
                        category: "Sapi Bali/Kupang/Madura",
                        weightView: "300-500 Kg",
                        priceView: "65.000 / Kg",
                        weightMin:300,
                        weightMax:500,
                        perKg: 65000
                    },
                    {
                        id: 6,
                        weightView: "500-800 Kg",
                        priceView: "70.000 / Kg",
                        weightMin:500,
                        weightMax:800,
                        perKg: 70000
                    },
                    {
                        id: 7,
                        weightView: "800 Kg - 1 Ton",
                        priceView: "75.000 / Kg",
                        weightMin:800,
                        weightMax:1000,
                        perKg: 75000
                    },
                    {
                        id: 8,
                        weightView:"Diatas 1 Ton",
                        priceView: "80.000 / Kg",
                        weightMin:1000,
                        perKg: 80000
                    }
                ]
            }
        },
        plan : {
            animal : null,
            weightView: null,
            weight: null,
            weightMin: null,
            weightMax: null,
            category: null,
            price: null,
            perKg: null
        },
        profile: {
            full_name: null,
            // legal_type: "",
            legal_id_no: null,
            phone: null
        },
        address: {
            province_id: 0,
            city_id: 0,
            district_id: 0,
            subdistrict_id: 0,
            detail: null,
            house_phone: null
        },
        state: {
            button: true,
            message:true
        }
    },

    methods: {
        getProvinces: function () {
            axios.get(url+'provinsi')
                .then(response => {
                    this.sources.provinces = response.data.provinsi;
                })
        },

        getCities: function (value) {
            axios.get(url+"kota?id_provinsi="+value)
                .then(response => {
                    this.sources.cities = response.data.kota_kabupaten;
                })
        },

        getDistricts: function (value) {
            axios.get(url+"kecamatan?id_kota="+value)
                .then(response => {
                    this.sources.districts = response.data.kecamatan;
                })
        },

        getSubdistricts: function (value) {
            axios.get(url+"kelurahan?id_kecamatan="+value)
                .then(response => {
                    this.sources.subdistricts = response.data.kelurahan;
                })
        },

        changeProvince: function changeItem(event) {
            this.districts = ""
            this.subdistricts = ""

            this.resetAddress()

            this.getCities(event.target.value);
        },

        changeCity: function changeItem(event) {
            this.subdistricts = ""

            this.getDistricts(event.target.value);
        },

        changeDistrict: function changeItem(event) {
            this.getSubdistricts(event.target.value);
        },

        resetAddress() {
            this.address.city_id = 0
            this.address.district_id = 0
            this.address.subdistrict_id = 0
        },

        selectKurban(price, perKg, weightMin, weightMax) {
            this.plan.price = null
            this.plan.perKg = null
            this.plan.weightMin = null
            this.plan.weightMax = null

            if(weightMin) {
                this.plan.weightMin = weightMin
            }

            if(weightMax) {
                this.plan.weightMax = weightMax
            }

            if (price) {
                this.plan.price = price
            } else if (perKg) {
                this.plan.perKg = perKg
            }

        },

        calculate() {
            this.plan.price = null;
            var weight = this.plan.weight ? parseInt(this.plan.weight) : 0

            this.plan.price = weight * parseInt(this.plan.perKg)

            if (this.plan.weightMin <= weight <= this.plan.weightMax) {
                this.state.button = false
                this.state.message = true
            } else {
                this.state.button = true
                this.state.message = false
            }
        },
    },
    computed:{
    },

    mounted: function() {
        this.getProvinces()
    }
});
