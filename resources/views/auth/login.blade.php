@section('title', 'Login Page')

<x-app-layout>
    <div class="wpo-login-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif
                    <form class="wpo-accountWrapper" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="wpo-accountInfo">
                            <div class="wpo-accountInfoHeader">
                                <a href="{{ route('user.home') }}"><img src="{{ asset('assets/images/logo.png') }}" alt=""></a>
                                    <a class="wpo-accountBtn" href="{{ route('register') }}">
                                        <span class="">Create Account</span>
                                    </a>
                            </div>
                            <div class="image">
                                <img src="{{ asset('assets/images/login.svg') }}" alt="">
                            </div>
                            <div class="back-home">
                                <a class="wpo-accountBtn" href="{{ route('user.home') }}">
                                    <span class="">Back To Home</span>
                                </a>
                            </div>
                        </div>
                        <div class="wpo-accountForm form-style">
                            <div class="fromTitle">
                                <x-jet-validation-errors class="mb-4" />

                                <h2>Login</h2>
                                <p>Sign into your pages account</p>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label>Email</label>
                                    <input type="text" id="email" name="email" placeholder="Masukkan email" value="{{ old('email') }}" required autofocus>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="form-group">
                                        <label >Password</label>
                                        <input class="pwd6" type="password" placeholder="Masukkan password" name="password" required autocomplete="current-password">
                                    </div>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default reveal6" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    </span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="check-box-wrap">
                                        <div class="input-box">
                                            <input type="checkbox" id="remember" name="remember" value="{{ old('remember') }}">
                                            <label for="remember">Remember Me</label>
                                        </div>
                                        @if (Route::has('password.request'))
                                            <div class="forget-btn">
                                                <a href="{{ route('password.request') }}">Forgot Password?</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <button type="submit" class="wpo-accountBtn">Login</button>
                                </div>
                            </div>
                            {{-- <h4 class="or"><span>OR</span></h4>
                            <ul class="wpo-socialLoginBtn">
                                <li><button class="facebook" tabindex="0" type="button"><span><i class="fa fa-facebook"></i></span></button></li>
                                <li><button class="twitter" tabindex="0" type="button"><span><i class="fa fa-twitter"></i></span></button></li>
                                <li><button class="linkedin" tabindex="0" type="button"><span><i class="fa fa-linkedin"></i></span></button></li>
                            </ul> --}}
                            <p class="subText">Don't have an account? <a href="{{ route('register') }}">Create free account</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
