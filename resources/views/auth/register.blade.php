@section('title', 'Register Page')

<x-app-layout>
    <div class="wpo-login-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form class="wpo-accountWrapper" method="POST" action="{{ route('register') }}">
                    @csrf
                        <div class="wpo-accountInfo">
                            <div class="wpo-accountInfoHeader">
                                <a href="{{ route('user.home') }}"><img src="assets/images/logo.png" alt=""></a>
                                <a class="wpo-accountBtn" href="{{ route('login') }}">
                                    <span class="">Log in</span>
                                </a>
                            </div>
                            <div class="image">
                                <img src="{{ asset('assets/images/login.svg') }}" alt="">
                            </div>
                            <div class="back-home">
                                <a class="wpo-accountBtn" href="{{ route('user.home') }}">
                                    <span class="">Back To Home</span>
                                </a>
                            </div>
                        </div>
                        <div class="wpo-accountForm form-style">
                            <div class="fromTitle">
                                <h2>Signup</h2>
                                <p>Sign into your pages account</p>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <x-jet-label value="{{ __('Nama Sesuai KTP') }}" />
                                    <x-jet-input class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <x-jet-label value="{{ __('Alamat Email') }}" />
                                    <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="form-group"><x-jet-label value="{{ __('Password') }}" />
                                        <x-jet-input class="block mt-1 w-full pwd2" type="password" name="password" required autocomplete="new-password" />
                                    </div>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default reveal3" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    </span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <div class="form-group"><x-jet-label value="{{ __('Confirm Password') }}" />
                                        <x-jet-input class="block mt-1 w-full pwd3" type="password" name="password_confirmation" required autocomplete="new-password" />
                                    </div>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default reveal2" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
                                    </span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-12">
                                    <button type="submit" class="wpo-accountBtn">{{ __('Register') }}</button>
                                </div>
                            </div>
                            {{-- <h4 class="or"><span>OR</span></h4> --}}
                            {{-- <ul class="wpo-socialLoginBtn">
                                <li><button class="facebook" tabindex="0" type="button"><span><i class="fa fa-facebook"></i></span></button></li>
                                <li><button class="twitter" tabindex="0" type="button"><span><i class="fa fa-twitter"></i></span></button></li>
                                <li><button class="linkedin" tabindex="0" type="button"><span><i class="fa fa-linkedin"></i></span></button></li>
                            </ul> --}}
                            <p class="subText">Have an account? <a href="{{ route('login') }}">Login</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

@section('js')
<script>
    $(".reveal").on('click', function() {
        var $pwd = $(".pwd");
        if ($pwd.attr('type') === 'text') {
            $pwd.attr('type', 'password');
        } else {
            $pwd.attr('type', 'text');
        }
    });
    $(".reveal2").on('click', function() {
        var $pwd = $(".pwdr");
        if ($pwd.attr('type') === 'text') {
            $pwd.attr('type', 'password');
        } else {
            $pwd.attr('type', 'text');
        }
    });
    </script>
@endsection
