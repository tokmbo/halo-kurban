@section('title', 'Kontak Halo Kurban')

@section('js')
    <script>
            const errors = @json($errors->all() ?: null);
    </script>
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/contact.js') }}"></script>
@endsection

<x-app-layout>
    <!-- start wpo-contact-form-map -->
    <section class="wpo-contact-form-map">
        <div class="container">
            <div class="row">
                <div class="col col-12 col-sm-12">
                    <div class="wpo-title-page-wrap">

                    <h2>Hubungi Kami</h2>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                            <div id="app" class="contact-form">
                                <h2>Isi Data Diri dan Pesan</h2>
                                <transition @@after-enter="slideHere" v-if="errors">
                                    <div class="alert alert-danger">
                                        <p v-for="error in errors">@{{ error.constructor === Array ? error[0] : error }}</p>
                                    </div>
                                </transition>
                                <transition @@after-enter="slideHere" v-if="success">
                                    <div class="alert alert-success">
                                        <p>Terima kasih, pesan telah terkirim.</p>
                                    </div>
                                </transition>
                                <form method="post" id="contact-form" class="contact-validation-active">
                                    <div>
                                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Nama Depan" v-model="message.firstname">
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="lastname" id="firstname" placeholder="Nama Belakang" v-model="message.lastname">
                                    </div>
                                    <div class="clearfix">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" v-model="message.email">
                                    </div>
                                    <div>
                                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Judul Pesan" v-model="message.subject">
                                    </div>
                                    <div>
                                        <textarea class="form-control" name="note" id="note" placeholder="Deskripsi masalah..." v-model="message.note"></textarea>
                                    </div>
                                    @csrf
                                    <div class="submit-area">
                                        <button type="button" @@click="submit" class="theme-btn submit-btn">Kirim Pesan</button>
                                        <div id="loader">
                                            <i class="ti-reload"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="contact-map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15863.555113603554!2d107.0341933!3d-6.2783506!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb5c34ef09aef85a5!2sHalo%20Ustadz!5e0!3m2!1sen!2sid!4v1600178897810!5m2!1sen!2sid" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="wpo-contact-info">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="info-item">
                                    <h2>Tambun Selatan, Bekasi</h2>
                                    <div class="info-wrap">
                                        <div class="info-icon">
                                            <i class="ti-world"></i>
                                        </div>
                                        <div class="info-text">
                                            <span>Alamat Kantor</span>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="info-item">
                                    <h2>infokita@haloustadz.id</h2>
                                    <div class="info-wrap">
                                        <div class="info-icon-2">
                                            <i class="fi flaticon-envelope"></i>
                                        </div>
                                        <div class="info-text">
                                            <span>Email Kantor</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="info-item">
                                    <h2>+628 588 310 8982</h2>
                                    <div class="info-wrap">
                                        <div class="info-icon-3">
                                            <i class="ti-headphone-alt"></i>
                                        </div>
                                        <div class="info-text">
                                            <span>Official Phone</span>
                                        </div>
                                    </div>
                                </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
    <!-- end wpo-contact-form-map -->
</x-app-layout>
