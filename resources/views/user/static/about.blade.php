@section('title', 'Tentang Halo Kurban')

<x-app-layout>
    <!-- wpo-about-video-area start -->
    <div class="wpo-about-video-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wpo-about-video-item">
                        <div class="wpo-about-video-img">
                            <img src="{{ asset('assets/images/about-vedio.jpg') }}" alt="">
                            <div class="entry-media video-holder">
                                <a href="https://www.youtube.com/embed/iSbzh0r9IV4?autoplay=1" class="video-btn" data-type="iframe">
                                    <i class=""></i>
                                </a>
                            </div>
                        </div>
                        <h2>Halo <span>Kurban</span></h2>
                        <center><p>Adalah lembaga pengelola hewan Sapi Potong atau Sapi Qurban sesuai dengan syarat dan syari’at Islam dalam pemeliharaannya. Insyaallah Kami hadir untuk menyediakan Sapi dengan kualitas baik tanpa menyalai aturan agama. kami menyediakan berbagai macam jenis sapi seperti sapi limosin, sapi kupang bali, sapi pegon, sapi simmental. Sehingga kami akan selalu berusaha memberikan sesuatu yang lebih dari Haknya kepada pembeli atau konsumen.</p></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wpo-about-video-area end -->
</x-app-layout>
