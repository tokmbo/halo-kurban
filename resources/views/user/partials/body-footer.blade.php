        <!-- start wpo-site-footer -->
        <footer class="wpo-site-footer">
            {{-- <div class="wpo-upper-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-3 col-md-3 col-sm-6">
                            <div class="widget about-widget">
                                <div class="logo widget-title">
                                    <img src="assets/images/logo-2.png" alt="blog">
                                </div>
                                <p>Build and Earn with your online store with lots of cool and exclusive wpo-features </p>
                                <ul>
                                    <li><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                    <li><a href="#"><i class="ti-instagram"></i></a></li>
                                    <li><a href="#"><i class="ti-google"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-md-3 col-sm-6">
                            <div class="widget link-widget resource-widget">
                                <div class="widget-title">
                                    <h3>Top News</h3>
                                </div>
                                <div class="news-wrap">
                                    <div class="news-img">
                                        <img src="assets/images/footer/img-1.jpg" alt="">
                                    </div>
                                    <div class="news-text">
                                        <h3>Education for all poor children</h3>
                                        <span>12 Nov, 2020</span>
                                    </div>
                                </div>
                                <div class="news-wrap">
                                    <div class="news-img">
                                        <img src="assets/images/footer/img-2.jpg" alt="">
                                    </div>
                                    <div class="news-text">
                                        <h3>Education for all poor children</h3>
                                        <span>12 Nov, 2020</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-2 col-md-3 col-sm-6">
                            <div class="widget link-widget">
                                <div class="widget-title">
                                    <h3>Useful Links</h3>
                                </div>
                                <ul>
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Our Causes</a></li>
                                    <li><a href="#">Our Mission</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Our Event</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col col-lg-3 col-lg-offset-1 col-md-3 col-sm-6">
                            <div class="widget market-widget wpo-service-link-widget">
                                <div class="widget-title">
                                    <h3>Contact </h3>
                                </div>
                                <p>online store with lots of cool and exclusive wpo-features</p>
                                <div class="contact-ft">
                                    <ul>
                                        <li><i class="fi flaticon-pin"></i>28 Street, New York City, USA</li>
                                        <li><i class="fi flaticon-call"></i>+000123456789</li>
                                        <li><i class="fi flaticon-envelope"></i>Hastium@gmail.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </div> --}}
            <div class="wpo-lower-footer">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <p class="copyright">&copy; 2020 Halo Ustadz. All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end wpo-site-footer -->
