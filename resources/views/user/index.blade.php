
@extends('user.layout')

@section('title', 'Index Halo Kurban')

@section('content')
        <!-- start of hero -->
        <section class="hero hero-style-2">
            <div class="hero-slider">
                <div class="slide">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-6 slide-caption">
                                <div class="slide-title">
                                    <h2>Halo<span>Kurban</span></h2>
                                </div>
                                <div class="slide-subtitle">
                                    <p>High Quality Charity Theme in Envato Market.</p>
                                    <p>You Can Satisfied Yourself By Helping.</p>
                                </div>
                                <div class="btns">
                                    {{-- <a href="donate.html" class="theme-btn">Donate Now</a> --}}
                                    <ul>
                                        <li class="video-holder">
                                            <a href="https://www.youtube.com/embed/4ZqbyTjiBqA?autoplay=1" class="video-btn" data-type="iframe"></a>
                                        </li>
                                        <li class="video-text">
                                            <a href="https://www.youtube.com/embed/4ZqbyTjiBqA?autoplay=1" class="video-btn" data-type="iframe">
                                                Putar Video
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-vec">
                    </div>
                </div>
            </div>
            <div class="hero-shape">
                <img src="{{ asset('assets/images/slider/shape.jpg') }}" alt="">
            </div>
        </section>
             <!-- world area start -->
        <div class="wpo-world-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-world-section">
                            <h2>Mulai tabung untuk kurban tahun depan</h2>
                            <a href="#"><img src="assets/images/team/1.png" alt="">Daftar Tabungan Kurban</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- world area end -->
        <!-- wpo-case-area start -->
        <div class="wpo-case-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <h2>Pasar Kurban</h2>
                            <span>Pilihan hewan kurban terbaikmu</span>
                        </div>
                    </div>
                </div>
                <div class="wpo-case-wrap">
                    <div class="wpo-case-slider">
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                <img src="{{ asset('images/kupang.jpeg') }}" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>sapi 30 jt / 7</h2>
                                        <ul>
                                            <li><span>Patungan:</span> 4.286.000</li>
                                        </ul>
                                        <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>6</span> Orang</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><span>Raised:</span> 25.000.000</li>
                                            <li><span>Goal:</span> 30.000.000</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="#">Lihat</a></li>
                                            <li><a href="donate.html">Gabung</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                    <img src="{{ asset('images/limosin.jpeg') }}" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>sapi 30 jt / 7</h2>
                                        <ul>
                                            <li><span>Patungan:</span> 4.286.000</li>
                                        </ul>
                                        <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>6</span> Orang</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul>
                                            <li><span>Raised:</span> 25.000.000</li>
                                            <li><span>Goal:</span> 30.000.000</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="#">Lihat</a></li>
                                            <li><a href="donate.html">Gabung</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                    <img src="{{ asset('images/pegon.jpeg') }}" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>sapi 30 jt / 7</h2>

                                        <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>6</span> Orang</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <ul>
                                            <li><span>Patungan:</span> 4.286.000</li>
                                            <li><span>Kuota:</span> 7 Orang</li>
                                        </ul>
                                        <ul>
                                            <li><span>Terkumpul:</span> 25.000.000</li>
                                            <li><span>Harga:</span> 30.000.000</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="#">Lihat</a></li>
                                            <li><a href="donate.html">Gabung</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-case-area end -->
        <!-- end of hero slider -->
         <!-- wpo-blog-area start -->
        <div class="wpo-blog-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <h2>Pilihan Paket</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="#">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 custom-grid">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/images/blog/img-1.jpg" alt="">
                                </div>
                                <div class="wpo-blog-content">
                                    <span></span>
                                    <h2>Tabungan Kurban</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 custom-grid">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/images/blog/img-2.jpg" alt="">
                                </div>
                                <div class="wpo-blog-content">
                                    <span></span>
                                    <h2>Pasar Kurban</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12 custom-grid">
                            <div class="wpo-blog-item">
                                <div class="wpo-blog-img">
                                    <img src="assets/images/blog/img-3.jpg" alt="">
                                </div>
                                <div class="wpo-blog-content">
                                    <span></span>
                                    <h2>Sodaqoh Kurban</h2>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- wpo-blog-area end -->
        <!-- wpo-mission-area start -->
        <div class="wpo-mission-area-2 section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <h2>Alasan Kenapa Kamu Harus Kurban di Halo Kurban</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-mission-wrap">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12 custom-grid">
                            <div class="wpo-mission-item">
                                <div class="wpo-mission-icon-5">
                                    <img src="assets/images/mission/icon1.png" alt="">
                                </div>
                                <div class="wpo-mission-content">
                                    <h2>Distribusi Ke Wilayah Membutuhkan</h2>
                                    <p>Lorem ipsum dolor amet cosectetur adipiscing, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 custom-grid">
                            <div class="wpo-mission-item">
                                <div class="wpo-mission-icon-6">
                                    <img src="assets/images/mission/icon2.png" alt="">
                                </div>
                                <div class="wpo-mission-content">
                                    <h2>Laporan Kurban Transparan</h2>
                                    <p>Lorem ipsum dolor amet cosectetur adipiscing, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 custom-grid">
                            <div class="wpo-mission-item">
                                <div class="wpo-mission-icon-7">
                                    <img src="assets/images/mission/icon3.png" alt="">
                                </div>
                                <div class="wpo-mission-content">
                                    <h2>Hewan Kurban Berkualitas</h2>
                                    <p>Lorem ipsum dolor amet cosectetur adipiscing, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-12 custom-grid">
                            <div class="wpo-mission-item">
                                <div class="wpo-mission-icon-8">
                                    <img src="assets/images/mission/icon4.png" alt="">
                                </div>
                                <div class="wpo-mission-content">
                                    <h2>Berdayakan Peternak dan Ekonomi Desa</h2>
                                    <p>Lorem ipsum dolor amet cosectetur adipiscing, sed do eiusmod.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-mission-area end -->

        <!-- .wpo-counter-area start -->
        <div class="wpo-counter-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wpo-counter-grids">
                            <div class="grid">
                                <div>
                                    <h2><span class="odometer" data-count="6200">00</span>+</h2>
                                </div>
                                <p>Shahibul Kurban</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h2><span class="odometer" data-count="80">00</span>+</h2>
                                </div>
                                <p>Hewan Kurban</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h2><span class="odometer" data-count="245">00</span>+</h2>
                                </div>
                                <p>Peternak</p>
                            </div>
                            <div class="grid">
                                <div>
                                    <h2><span class="odometer" data-count="605">00</span>+</h2>
                                </div>
                                <p>Rekanan Distribusi</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .wpo-counter-area end -->
@endsection
