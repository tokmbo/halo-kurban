@section('title', 'Tentang Halo Kurban')

@section('title', 'Index Halo Kurban')

@section('js')
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script>
        const tes = @json('parent' ?? null);
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    
@endsection
<x-app-layout>
             <!-- start of hero -->
        <section class="hero hero-style-2">
            <div class="hero-slider">
                <div class="slide">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-6 slide-caption">
                                <div class="slide-title">
                                    <h2>Let’s be Kind for <span>Children</span></h2>
                                </div>
                                <div class="slide-subtitle">
                                    <p>High Quality Charity Theme in Envato Market.</p>
                                    <p>You Can Satisfied Yourself By Helping.</p>
                                </div>
                                <div class="btns">
                                    <a href="donate.html" class="theme-btn">Donate Now</a>
                                    <ul>
                                        <li class="video-holder">
                                            <a href="https://www.youtube.com/embed/iSbzh0r9IV4?autoplay=1" class="video-btn" data-type="iframe"></a>
                                        </li>
                                        <li class="video-text">
                                            <a href="https://www.youtube.com/embed/iSbzh0r9IV4?autoplay=1" class="video-btn" data-type="iframe">
                                                Watch Our Video
                                            </a>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-vec">
                    </div>
                </div>
            </div>
            <div class="hero-shape">
                <img src="assets/images/slider/shape.jpg" alt="">
            </div>
        </section>
        <!-- end of hero slider -->
        <!-- wpo-case-area start -->
        <div class="wpo-case-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wpo-section-title">
                            <span>DAFTAR HARGA</span>
                            <h2>HEWAN QURBAN TAHUN 2021/1442 H</h2>
                        </div>
                    </div>
                </div>
                <div class="wpo-case-wrap">
                    <div class="wpo-case-slider">
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                    <img src="assets/images/case/img-1.png" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>Domba Betina</h2>
                                        {{-- <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>65.5</span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <ul>
                                            <li><span>Raised:</span> $7,000.00</li>
                                            <li><span>Goal:</span> $8,000.00</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="/detilkurban">Learn More</a></li>
                                            <li><a href="#">Kurban Sekarang</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                    <img src="assets/images/case/img-2.png" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>Domba Jantan</h2>
                                        {{-- <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>40.5</span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <ul>
                                            <li><span>Raised:</span> $7,000.00</li>
                                            <li><span>Goal:</span> $8,000.00</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="/detilkurban">Learn More</a></li>
                                            <li><a href="#">Kurban Sekarang</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpo-case-single">
                            <div class="wpo-case-item">
                                <div class="wpo-case-img">
                                    <img src="assets/images/case/img-3.png" alt="">
                                </div>
                                <div class="wpo-case-content">
                                    <div class="wpo-case-text-top">
                                        <h2>Sapi bali/tar</h2>
                                       {{--  <div class="progress-section">
                                            <div class="process">
                                                <div class="progress">
                                                    <div class="progress-bar">
                                                        <div class="progress-value"><span>80.5</span>%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <ul>
                                            <li><span>Raised:</span> $7,000.00</li>
                                            <li><span>Goal:</span> $8,000.00</li>
                                        </ul>
                                    </div>
                                    <div class="case-btn">
                                        <ul>
                                            <li><a href="/detilkurban">Learn More</a></li>
                                            <li><a href="#">Kurban Sekarang</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-case-area end -->
</x-app-layout>
