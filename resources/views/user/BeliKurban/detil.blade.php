@section('title', 'Tentang Halo Kurban')

@section('title', 'Index Halo Kurban')

@section('js')
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script>
        const tes = @json('parent' ?? null);
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    
@endsection
<x-app-layout>
        <!-- wpo-event-details-area start -->
        <div class="wpo-case-details-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-8">
                        <div class="wpo-case-details-wrap">
                            <div class="wpo-case-details-img">
                                <img src="assets/images/event-details.jpg" alt="">
                            </div>
                            <div class="wpo-case-details-tab">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#Description">Description</a></li>
                                    <li><a data-toggle="tab" href="#Donations">Donations</a></li>
                                    <li><a data-toggle="tab" href="#Comments">Comments</a></li>
                                </ul>
                            </div>
                            <div class="wpo-case-details-text">
                                <div class="tab-content">
                                    <div id="Description" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="wpo-case-content">
                                                    <div class="wpo-case-text-top">
                                                        <h2>Ensure Education for every poor children</h2>
                                                        <div class="progress-section">
                                                            <div class="process">
                                                                <div class="progress">
                                                                    <div class="progress-bar">
                                                                        <div class="progress-value"><span>65.5</span>%</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul>
                                                            <li><span>Raised:</span> $7,000.00</li>
                                                            <li><span>Goal:</span> $8,000.00</li>
                                                            <li><span>Donar:</span> 380</li>
                                                        </ul>
                                                        <div class="case-b-text">
                                                            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.</p>
                                                            <p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.</p>
                                                            <p>But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures.</p>
                                                        </div>
                                                        <div class="case-bb-text">
                                                            <h3>We want to ensure the education for the kids.</h3>
                                                            <p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure.</p>
                                                            <ul>
                                                                <li>The wise man therefore always holds in these matters.</li>
                                                                <li>In a free hour, when our power of choice and when nothing.</li>
                                                                <li>Else he endures pains to avoid worse pains.</li>
                                                                <li>We denounce with righteous indignation and dislike men. </li>
                                                                <li>Which is the same as saying through.</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Comments" class="tab-pane wpo-blog-single-section">
                                        <div class="comments-area">
                                            <div class="comments-section">
                                                <h3 class="comments-title">Comments</h3>
                                                <ol class="comments">
                                                    <li class="comment even thread-even depth-1" id="comment-1">
                                                        <div id="div-comment-1">
                                                            <div class="comment-theme">
                                                                <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-1.jpg" alt></div>
                                                            </div>
                                                            <div class="comment-main-area">
                                                                <div class="comment-wrapper">
                                                                    <div class="comments-meta">
                                                                        <h4>John Abraham <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                    </div>
                                                                    <div class="comment-area">
                                                                        <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                        <div class="comments-reply">
                                                                            <a class="comment-reply-link" href="#"><i class="fa fa-reply" aria-hidden="true"></i><span>Reply</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul class="children">
                                                            <li class="comment">
                                                                <div>
                                                                    <div class="comment-theme">
                                                                        <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-2.jpg" alt></div>
                                                                    </div>
                                                                    <div class="comment-main-area">
                                                                        <div class="comment-wrapper">
                                                                            <div class="comments-meta">
                                                                                <h4>Lily Watson <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                            </div>
                                                                            <div class="comment-area">
                                                                                <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                                <div class="comments-reply">
                                                                                    <a class="comment-reply-link" href="#"><span><i class="fa fa-reply" aria-hidden="true"></i> Reply</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <ul>
                                                                    <li class="comment">
                                                                        <div>
                                                                            <div class="comment-theme">
                                                                                <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-3.jpg" alt></div>
                                                                            </div>
                                                                            <div class="comment-main-area">
                                                                                <div class="comment-wrapper">
                                                                                    <div class="comments-meta">
                                                                                        <h4>John Abraham <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                                    </div>
                                                                                    <div class="comment-area">
                                                                                        <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                                        <div class="comments-reply">
                                                                                            <a class="comment-reply-link" href="#"><span><i class="fa fa-reply" aria-hidden="true"></i> Reply</span></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ol>
                                            </div> <!-- end comments-section -->
                                        </div> <!-- end comments-area -->
                                        <div class="comment-respond">
                                            <h3 class="comment-reply-title">Leave a Comment</h3>
                                            <form method="post" id="commentform" class="comment-form">
                                                <div class="form-inputs">
                                                    <input placeholder="Name" type="text">
                                                    <input placeholder="Email" type="email">
                                                    <input placeholder="Website" type="url">
                                                </div>
                                                <div class="form-textarea">
                                                    <textarea id="comment" placeholder="Write Your Comments..."></textarea>
                                                </div>
                                                <div class="form-submit">
                                                    <input id="submit" value="Reply" type="submit">
                                                </div>
                                            </form>
                                        </div>
                                    </div>                                
                                    <div id="Donations" class="tab-pane">
                                        <form action="#">
                                            <div class="wpo-donations-amount">
                                                <h2>Your Donation</h2>
                                                <input type="text" class="form-control" name="text" id="text" placeholder="Enter Donation Amount">
                                            </div>
                                            <div class="wpo-donations-details">
                                                <h2>Details</h2>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="name" id="fname" placeholder="First Name">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Last Name">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group clearfix">
                                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="Adress" id="Adress" placeholder="Adress">
                                                    </div>
                                                    <div class="col-lg-12 col-12 form-group">
                                                        <textarea class="form-control" name="note"  id="note" placeholder="Message"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpo-doanation-payment">
                                                <h2>Choose Your Payment Method</h2>
                                                <div class="wpo-payment-area">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="wpo-payment-option" id="open4">
                                                                <div class="wpo-payment-select">
                                                                    <ul> 
                                                                        <li class="addToggle">
                                                                            <input id="add" checked="checked"  type="radio" name="payment" value="30">
                                                                            <label for="add">Payment By Card</label>
                                                                        </li>
                                                                        <li class="removeToggle">
                                                                            <input id="remove" type="radio" name="payment" value="30">
                                                                            <label for="remove">Offline Donation</label>
                                                                        </li>
                                                                    </ul>
                                                               </div>
                                                                <div id="open5" class="payment-name">
                                                                    <ul> 
                                                                        <li class="visa"><input id="1" type="radio" name="size" value="30">
                                                                            <label for="1"><img src="assets/images/checkout/img-1.png" alt=""></label>
                                                                        </li>
                                                                        <li class="mas"><input id="2" type="radio" name="size" value="30">
                                                                            <label for="2"><img src="assets/images/checkout/img-2.png" alt=""></label>
                                                                        </li>
                                                                        <li class="ski"><input id="3" type="radio" name="size" value="30">
                                                                            <label for="3"><img src="assets/images/checkout/img-3.png" alt=""></label>
                                                                        </li>
                                                                        <li class="pay"><input id="4" type="radio" name="size" value="30">
                                                                            <label for="4"><img src="assets/images/checkout/img-4.png" alt=""></label>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="contact-form form-style">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Card holder Name</label>
                                                                                <input type="text" placeholder=""  name="name">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Card Number</label>
                                                                                <input type="text" placeholder="" id="card" name="card">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>CVV</label>
                                                                                <input type="text" placeholder="" name="CVV">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Expire Date</label>
                                                                                <input type="text" placeholder=""  name="date">
                                                                            </div>
                                                                        </div>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="submit-area">
                                                <button type="submit" class="theme-btn submit-btn">Donate Now</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="wpo-blog-sidebar">
                            <div class="widget search-widget">
                                <h3>Search Here</h3>
                                <form>
                                    <div>
                                        <input type="text" class="form-control" placeholder="Search Post..">
                                        <button type="submit"><i class="ti-search"></i></button>
                                    </div>
                                </form>
                            </div>
                            <div class="widget recent-post-widget">
                                <h3>Recent posts</h3>
                                <div class="posts">
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-1.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Many Children are suffering a lot for food.</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-2.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Be kind for the poor people and the kids.</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-3.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Be soft and kind for the poor people.</a></h4>
                                            <span class="date">22 Sep 2020</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget tag-widget">
                                <h3>Tags</h3>
                                <ul>
                                    <li><a href="#">Donations</a></li>
                                    <li><a href="#">Charity</a></li>
                                    <li><a href="#">Help</a></li>
                                    <li><a href="#">Non Profit</a></li>
                                    <li><a href="#">Poor People</a></li>
                                    <li><a href="#">Helping Hand</a></li>
                                    <li><a href="#">Video</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-event-details-area end -->
</x-app-layout>
