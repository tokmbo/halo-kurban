@section('title', 'Edit Account')

@section('js')
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/editaccount.js') }}"></script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var ktp = document.getElementById('legal_id_no').value;
            var charCode = (evt.which) ? evt.which : evt.keyCode

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
                if (ktp.length > 16)
                    return false;
            return true;
        }
    </script>
@endsection

<x-app-layout>
    <!-- wpo-event-area start -->
    <div class="wpo-donation-page-area section-padding">
        <div class="container" id="app">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wpo-donate-header">
                        <h2>Pendaftaran Rekening Tabungan Kurban</h2>
                    </div>
                    <div class="tab-pane">
                        <form action="{{ route('user.account.edit') }}" method="post">
                            @csrf
                            <div class="wpo-donations-amount">
                                <h2>Data Pribadi</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <input type="text" class="form-control" v-model="profile.full_name" name="full_name" placeholder="Nama Lengkap">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <input type="number" id="legal_id_no" class="form-control" v-model="profile.legal_id_no" name="legal_id_no" placeholder="Nomor Identitas" onkeypress="return isNumberKey(event)" maxlength="16">
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <input type="text" class="form-control" name="phone" v-model="profile.phone" placeholder="Nomor HP">
                                    </div>
                                </div>
                            </div>
                            <div class="wpo-donations-amount">
                                <h2>Data Alamat</h2>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <select class="form-control" v-on:change="changeProvince($event)" placeholder="Alamat Lengkap" v-model="address.province_id" name="province_id">
                                            <option value="0">Pilih Provinsi</option>
                                            <option v-for="provinsi in sources.provinces" v-bind:value="provinsi.id" v-bind:id="provinsi.id" >@{{ provinsi.nama }}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <select class="form-control" v-on:change="changeCity($event)" v-model="address.city_id" placeholder="Alamat Lengkap" name="city_id">
                                            <option value="0">Pilih Kota/Kabupaten</option>
                                            <option v-for="kota in sources.cities" v-bind:value="kota.id" v-bind:id="kota.id" >@{{ kota.nama }}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <select class="form-control" v-on:change="changeDistrict($event)" placeholder="Alamat Lengkap" v-model="address.district_id" name="district_id">
                                            <option value="0">Pilih Kecamatan</option>
                                            <option v-for="kecamatan in sources.districts" v-bind:value="kecamatan.id" v-bind:id="kecamatan.id" >@{{ kecamatan.nama }}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <select class="form-control" placeholder="Alamat Lengkap" v-model="address.subdistrict_id" name="subdistrict_id">
                                            <option value="0">Pilih Kelurahan</option>
                                            <option v-for="kelurahan in sources.subdistricts" v-bind:value="kelurahan.id" v-bind:id="kelurahan.id" >@{{ kelurahan.nama }}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <input type="text" class="form-control" name="detail" v-model="address.detail" placeholder="Detail Alamat (Jl, / RT/RW)">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <input type="text" class="form-control" name="house_phone" v-model="address.house_phone" placeholder="No.Telepon Rumah">
                                    </div>
                                </div>
                            </div>

                            <div class="wpo-donations-amount">
                                <h2>Target Hewan Kurban Tahun Depan</h2>
                                <div class="wpo-payment-area">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="wpo-payment-option" id="open4">
                                                <div id="open5" class="payment-name">
                                                    <ul>
                                                        <li class="visa"><input id="animal-1" v-model="plan.animal" type="radio" name="animal" value="Kambing">
                                                            <label for="animal-1"><img src="assets/images/checkout/img-1.png" alt=""></label>
                                                        </li>
                                                        <li class="mas"><input id="animal-2" v-model="plan.animal" type="radio" name="animal" value="Sapi">
                                                            <label for="animal-2"><img src="assets/images/checkout/img-2.png" alt=""></label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" v-if="plan.animal">
                                        <div class="col-12">
                                            <div class="wpo-payment-option" id="open4">
                                                <div id="open5" class="payment-name">
                                                    <ul v-if="plan.animal == 'Sapi'">
                                                        <li v-for="animal in sources.animals.sapi" class="visa"><input :id="animal.id" v-model="plan.weightView" @@change="selectKurban(animal.price, animal.perKg, animal.weightMin, animal.weightMax,)" type="radio" name="weight" :value="animal.weightView">
                                                            <label :for="animal.id"><img src="assets/images/checkout/img-1.png" alt=""></label>

                                                        </li>
                                                    </ul>
                                                    <ul v-else="plan.animal == 'Kambing'">
                                                        <li v-for="animal in sources.animals.kambing" class="visa"><input :id="animal.id" v-model="plan.weightView"  @@change="selectKurban(animal.price, animal.perKg)" type="radio" name="weight" :value="animal.weightView">
                                                            <label :for="animal.id"><img src="assets/images/checkout/img-1.png" alt=""></label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 20px" v-if="plan.animal">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-4 form-group" v-if="plan.perKg">
                                            <input type="number" class="form-control" @@input="calculate()" v-model="plan.weight" placeholder="Masukkan berat (Kg)" :min="plan.weightMin" :max="plan.weightMax">
                                            <label v-if="state.message">Berat antara @{{ plan.weightMin }} dan @{{ plan.weightMax }}</label>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 20px">

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                            <blockquote class="blockquote">
                                                <label>Target Hewan Kurban</label>
                                                <p class="mb-0">Berikut ini adalah kalkulasi target tabungan hewan kurban</p>
                                                <dl class="row">
                                                    <dt class="col-sm-3">Jenis Hewan</dt>
                                                    <dd class="col-sm-9" >: @{{ plan.animal }}</dd>
                                                    <dt class="col-sm-3">Berat Sekitar</dt>
                                                    <dd v-if="plan.perKg" class="col-sm-9">: @{{ plan.weight }} Kg</dd>
                                                    <dd v-else class="col-sm-9">: @{{ plan.weightView }}</dd>
                                                    <dt class="col-sm-3">Harga</dt>
                                                    <dd v-if="plan.perKg" class="col-sm-9">: @{{ plan.perKg }} /Kg x @{{ plan.weight }} Kg = @{{ plan.price }}</dd>
                                                    <dd v-else class="col-sm-9">: @{{ plan.price }}</dd>
                                                </dl>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="submit-area">
                                <button type="submit" class="theme-btn submit-btn" :disabled="state.button == false">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wpo-event-area end -->
</x-app-layout>

