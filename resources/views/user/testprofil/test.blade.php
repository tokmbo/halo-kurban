@section('title', 'Tentang Halo Kurban')

@section('title', 'Index Halo Kurban')

@section('js')
    <script src="{{ asset('js/manifest.js') }}"></script>
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script>
        const tes = @json('parent' ?? null);
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
@endsection
<x-app-layout>
        <!-- wpo-event-details-area start -->
        <div class="wpo-case-details-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-md-8">
                        <div class="wpo-case-details-wrap">
                            <div class="wpo-case-details-img">
                                <img src="{{ asset('images/pegon.jpeg') }}" alt="">
                            </div>
                            <div class="wpo-case-details-tab">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#Description">Description</a></li>
                                    <li><a data-toggle="tab" href="#Donations">Bergabung</a></li>
                                    <li><a data-toggle="tab" href="#Comments">Comments</a></li>
                                </ul>
                            </div>
                            <div class="wpo-case-details-text">
                                <div class="tab-content">
                                    <div id="Description" class="tab-pane active">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="wpo-case-content">
                                                    <div class="wpo-case-text-top">
                                                        <h2>{{ $animal->name }}</h2>
                                                        <div class="progress-section">
                                                            <div class="process">
                                                                <div class="progress">
                                                                    <div class="progress-bar">
                                                                        <div class="progress-value"><span>65.5</span>%</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul>
                                                            <li><span>Raised:</span> $7,000.00</li>
                                                            <li><span>Goal:</span> $8,000.00</li>
                                                            <li><span>Donar:</span> 380</li>
                                                        </ul>
                                                        <div class="case-b-text">
                                                            <p>{!! $animal->description !!}</p>
                                                        </div>
                                                        {{-- <div class="case-bb-text">
                                                            <h3>We want to ensure the education for the kids.</h3>
                                                            <p>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure.</p>
                                                            <ul>
                                                                <li>The wise man therefore always holds in these matters.</li>
                                                                <li>In a free hour, when our power of choice and when nothing.</li>
                                                                <li>Else he endures pains to avoid worse pains.</li>
                                                                <li>We denounce with righteous indignation and dislike men. </li>
                                                                <li>Which is the same as saying through.</li>
                                                            </ul>
                                                        </div> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Comments" class="tab-pane wpo-blog-single-section">
                                        <div class="comments-area">
                                            <div class="comments-section">
                                                <h3 class="comments-title">Comments</h3>
                                                <ol class="comments">
                                                    <li class="comment even thread-even depth-1" id="comment-1">
                                                        <div id="div-comment-1">
                                                            <div class="comment-theme">
                                                                <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-1.jpg" alt></div>
                                                            </div>
                                                            <div class="comment-main-area">
                                                                <div class="comment-wrapper">
                                                                    <div class="comments-meta">
                                                                        <h4>John Abraham <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                    </div>
                                                                    <div class="comment-area">
                                                                        <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                        <div class="comments-reply">
                                                                            <a class="comment-reply-link" href="#"><i class="fa fa-reply" aria-hidden="true"></i><span>Reply</span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <ul class="children">
                                                            <li class="comment">
                                                                <div>
                                                                    <div class="comment-theme">
                                                                        <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-2.jpg" alt></div>
                                                                    </div>
                                                                    <div class="comment-main-area">
                                                                        <div class="comment-wrapper">
                                                                            <div class="comments-meta">
                                                                                <h4>Lily Watson <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                            </div>
                                                                            <div class="comment-area">
                                                                                <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                                <div class="comments-reply">
                                                                                    <a class="comment-reply-link" href="#"><span><i class="fa fa-reply" aria-hidden="true"></i> Reply</span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <ul>
                                                                    <li class="comment">
                                                                        <div>
                                                                            <div class="comment-theme">
                                                                                <div class="comment-image"><img src="assets/images/blog-details/comments-author/img-3.jpg" alt></div>
                                                                            </div>
                                                                            <div class="comment-main-area">
                                                                                <div class="comment-wrapper">
                                                                                    <div class="comments-meta">
                                                                                        <h4>John Abraham <span class="comments-date">Octobor 28,2018 At 9.00am</span></h4>
                                                                                    </div>
                                                                                    <div class="comment-area">
                                                                                        <p>I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, </p>
                                                                                        <div class="comments-reply">
                                                                                            <a class="comment-reply-link" href="#"><span><i class="fa fa-reply" aria-hidden="true"></i> Reply</span></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ol>
                                            </div> <!-- end comments-section -->
                                        </div> <!-- end comments-area -->
                                        <div class="comment-respond">
                                            <h3 class="comment-reply-title">Leave a Comment</h3>
                                            <form method="post" id="commentform" class="comment-form">
                                                <div class="form-inputs">
                                                    <input placeholder="Name" type="text">
                                                    <input placeholder="Email" type="email">
                                                    <input placeholder="Website" type="url">
                                                </div>
                                                <div class="form-textarea">
                                                    <textarea id="comment" placeholder="Write Your Comments..."></textarea>
                                                </div>
                                                <div class="form-submit">
                                                    <input id="submit" value="Reply" type="submit">
                                                </div>
                                            </form>
                                        </div>
                                    </div>                                
                                    <div id="Donations" class="tab-pane">
                                        <form action="#">
                                            <div class="wpo-donations-amount">
                                                <h2>Your Donation</h2>
                                                <input type="text" class="form-control" name="text" id="text" placeholder="Enter Donation Amount">
                                            </div>
                                            <div class="wpo-donations-details">
                                                <h2>Details</h2>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="name" id="fname" placeholder="First Name">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Last Name">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group clearfix">
                                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-12 form-group">
                                                        <input type="text" class="form-control" name="Adress" id="Adress" placeholder="Adress">
                                                    </div>
                                                    <div class="col-lg-12 col-12 form-group">
                                                        <textarea class="form-control" name="note"  id="note" placeholder="Message"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpo-doanation-payment">
                                                <h2>Choose Your Payment Method</h2>
                                                <div class="wpo-payment-area">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="wpo-payment-option" id="open4">
                                                                <div class="wpo-payment-select">
                                                                    <ul> 
                                                                        <li class="addToggle">
                                                                            <input id="add" checked="checked"  type="radio" name="payment" value="30">
                                                                            <label for="add">Payment By Card</label>
                                                                        </li>
                                                                        <li class="removeToggle">
                                                                            <input id="remove" type="radio" name="payment" value="30">
                                                                            <label for="remove">Offline Donation</label>
                                                                        </li>
                                                                    </ul>
                                                               </div>
                                                                <div id="open5" class="payment-name">
                                                                    <ul> 
                                                                        <li class="visa"><input id="1" type="radio" name="size" value="30">
                                                                            <label for="1"><img src="assets/images/checkout/img-1.png" alt=""></label>
                                                                        </li>
                                                                        <li class="mas"><input id="2" type="radio" name="size" value="30">
                                                                            <label for="2"><img src="assets/images/checkout/img-2.png" alt=""></label>
                                                                        </li>
                                                                        <li class="ski"><input id="3" type="radio" name="size" value="30">
                                                                            <label for="3"><img src="assets/images/checkout/img-3.png" alt=""></label>
                                                                        </li>
                                                                        <li class="pay"><input id="4" type="radio" name="size" value="30">
                                                                            <label for="4"><img src="assets/images/checkout/img-4.png" alt=""></label>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="contact-form form-style">
                                                                        <div class="row">
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Card holder Name</label>
                                                                                <input type="text" placeholder=""  name="name">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Card Number</label>
                                                                                <input type="text" placeholder="" id="card" name="card">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>CVV</label>
                                                                                <input type="text" placeholder="" name="CVV">
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-12 col-12">
                                                                                <label>Expire Date</label>
                                                                                <input type="text" placeholder=""  name="date">
                                                                            </div>
                                                                        </div>
                                                                   </div>
                                                               </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="submit-area">
                                                <button type="submit" class="theme-btn submit-btn">Donate Now</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="wpo-blog-sidebar">
                            <div class="widget tag-widget">
                                <h1>{{ $animal->name }}</h1>
                                <h2>Rp. {{ $animal->price }}</h2>
                                <h3>Status : In stock</h3>
                                <ul>
                                    <li><a href="#">Beli Sekarang</a></li>
                                    <li><a href="#">+ Keranjang</a></li>
                                </ul>
                            </div>
                            <div class="widget recent-post-widget">
                                <h3>Pilihan Hewan Kurban Lainnya</h3>
                                <div class="posts">
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-1.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Sapi 1/7</a></h4>
                                            <span class="date">Rp 2.800.000</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-2.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Domba betina</a></h4>
                                            <span class="date">Rp 2.800.000</span>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <div class="img-holder">
                                            <img src="assets/images/recent-posts/img-3.jpg" alt>
                                        </div>
                                        <div class="details">
                                            <h4><a href="#">Domba Jantan</a></h4>
                                            <span class="date">Rp 2.800.000</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget tag-widget">
                                <h3>Tags</h3>
                                <ul>
                                    <li><a href="#">Donations</a></li>
                                    <li><a href="#">Charity</a></li>
                                    <li><a href="#">Help</a></li>
                                    <li><a href="#">Non Profit</a></li>
                                    <li><a href="#">Poor People</a></li>
                                    <li><a href="#">Helping Hand</a></li>
                                    <li><a href="#">Video</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- wpo-event-details-area end -->
        <div class="wpo-ne-footer">
            <!-- start wpo-news-letter-section -->
            <section class="wpo-news-letter-section">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <div class="wpo-newsletter">
                                <h3>Follow us For ferther information</h3>
                                <div class="wpo-newsletter-form">
                                    <form>
                                        <div>
                                            <input type="text" placeholder="Enter Your Email" class="form-control">
                                            <button type="submit">Subscribe</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end container -->
            </section>
            <!-- end wpo-news-letter-section -->
            <!-- start wpo-site-footer -->
            <footer class="wpo-site-footer">
                <div class="wpo-upper-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col col-lg-3 col-md-3 col-sm-6">
                                <div class="widget about-widget">
                                    <div class="logo widget-title">
                                        <img src="assets/images/logo-2.png" alt="blog">
                                    </div>
                                    <p>Build and Earn with your online store with lots of cool and exclusive wpo-features </p>
                                    <ul>
                                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                                        <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                                        <li><a href="#"><i class="ti-google"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-lg-3 col-md-3 col-sm-6">
                                <div class="widget link-widget resource-widget">
                                    <div class="widget-title">
                                        <h3>Top News</h3>
                                    </div>
                                    <div class="news-wrap">
                                        <div class="news-img">
                                            <img src="assets/images/footer/img-1.jpg" alt="">
                                        </div>
                                        <div class="news-text">
                                            <h3>Education for all poor children</h3>
                                            <span>12 Nov, 2020</span>
                                        </div>
                                    </div>
                                    <div class="news-wrap">
                                        <div class="news-img">
                                            <img src="assets/images/footer/img-2.jpg" alt="">
                                        </div>
                                        <div class="news-text">
                                            <h3>Education for all poor children</h3>
                                            <span>12 Nov, 2020</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-lg-2 col-md-3 col-sm-6">
                                <div class="widget link-widget">
                                    <div class="widget-title">
                                        <h3>Useful Links</h3>
                                    </div>
                                    <ul>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Our Causes</a></li>
                                        <li><a href="#">Our Mission</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                        <li><a href="#">Our Event</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col col-lg-3 col-lg-offset-1 col-md-3 col-sm-6">
                                <div class="widget market-widget wpo-service-link-widget">
                                    <div class="widget-title">
                                        <h3>Contact </h3>
                                    </div>
                                    <p>online store with lots of cool and exclusive wpo-features</p>
                                    <div class="contact-ft">
                                        <ul>
                                            <li><i class="fi flaticon-pin"></i>28 Street, New York City, USA</li>
                                            <li><i class="fi flaticon-call"></i>+000123456789</li>
                                            <li><i class="fi flaticon-envelope"></i>Hastium@gmail.com</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end container -->
                </div>
            </footer>
            <!-- end wpo-site-footer -->
        </div>

</x-app-layout>
