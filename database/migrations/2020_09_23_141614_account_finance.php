<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AccountFinance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('account_finance', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('salary');
            $table->string('source_of_income');
            $table->string('account_type');
            $table->string('nominal_deposit');
            $table->string('opening_account_city');
            $table->string('opening_account_city_unit');
            $table->string('opening_account_city_unit_address');
            $table->string('opening_account_purpose');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_finance');
    }
}
