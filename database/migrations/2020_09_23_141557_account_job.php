<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AccountJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('account_job', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('type_job');
            $table->string('npwp_no')->unique();
            $table->string('company_postal');
            $table->string('company_no_code');
            $table->string('company_no');
            $table->string('company_fax');
            $table->string('province');
            $table->string('position');
            $table->string('company_name');
            $table->string('company_address');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
